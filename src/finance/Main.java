
package finance;

import finance.modelo.*;
import finance.vistas.Vlogin;

public class Main {

    public static void main(String[] args) {
        //REGLAS DE LA FINANCIERA
        Financiera.getInstance().setNombreComercial("FINAN");
        Financiera.getInstance().setRazonSocial("RAZON SOCIAL");
        Financiera.getInstance().setCuit(2034532359);
        Financiera.getInstance().setDomicilio("Av. Siria 2000");
        Financiera.getInstance().setMontoMaximo(100000);
        Financiera.getInstance().setInteresDiario(5);
        Financiera.getInstance().setCantidadMaximaCreditosActivos(3);
        Financiera.getInstance().setCantidadCuotasImpagas(2);
        new PlanCuotaVencida().setPorcentajeGastoAdmninistrativo(2);
        Repositorio.getInstance().crearPlanes();
        new Vlogin().ejecutar();
        
        
    }
    
}
