
package finance.Interface;

import finance.modelo.Pago;

public interface IAbonarCuota {
    void informarCreditoFinalizado(int dni);
    void abonarCuota(Pago pago);
    void abonarCuotaParcial(double monto);
    void MostrarMensaje(String mje);
}
