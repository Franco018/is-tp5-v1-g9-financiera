
package finance.Interface;

import finance.modelo.Cliente;

public interface IRegistrarCliente {
    
    void RegistrarCliente(Cliente cliente);
    void MostrarMensaje(String text);
    void Modificar();
    void Eliminar();
}
