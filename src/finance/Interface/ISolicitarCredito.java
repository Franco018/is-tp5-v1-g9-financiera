
package finance.Interface;

import finance.modelo.*;
import java.util.ArrayList;

public interface ISolicitarCredito {
    void ObtenerEstadoCliente(int dni);
    void cargarPlanes(ArrayList<Plan> planes);
    void OtorgarCredito(String descripcionPlan, Credito credito);
    void MostrarMensaje(String txt);
}
