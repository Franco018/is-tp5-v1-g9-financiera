
package finance.Interface;

import finance.modelo.Empleado;

public interface IRegistrarEmpleado {
    void RegistrarEmpleado(Empleado empleado);
    void MostrarMensaje(String text);
    void Modificar();
    void Eliminar();
}
