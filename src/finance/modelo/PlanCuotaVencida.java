
package finance.modelo;

public class PlanCuotaVencida extends Plan {
    
    public double porcentajeGastoAdmninistrativo;
    
    public PlanCuotaVencida(int cuota, double porcentajePlan, String modalidadPago) {
        super(cuota, porcentajePlan, modalidadPago);
    }

    public PlanCuotaVencida() {
    }
    
    public double getPorcentajeGastoAdmninistrativo() {
        return porcentajeGastoAdmninistrativo;
    }

    public void setPorcentajeGastoAdmninistrativo(double porcentajeGastoAdmninistrativo) {
        this.porcentajeGastoAdmninistrativo = porcentajeGastoAdmninistrativo;
    }
    
    
}
