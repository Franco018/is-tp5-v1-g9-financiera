
package finance.modelo;

import java.util.ArrayList;

public class Plan {
    private int nroPlan;
    private String descripcion;
    private int cantidadCuotas;
    private double porcentajePlanMensual;
    private ArrayList<Credito> _creditos = new ArrayList<>(); //Agregue

    public Plan() {
    }
    
    public Plan(int Cantcuota, double porcentajePlan, String descripcion) {
        this.cantidadCuotas = Cantcuota;
        this.porcentajePlanMensual = porcentajePlan;
        this.descripcion = descripcion;
    }
    
    public void agregarCredito(Credito c){
        _creditos.add(c);
    }
    
    public int getNroPlan() {
        return nroPlan;
    }

    public void setNroPlan(int nroPlan) {
        this.nroPlan = nroPlan;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCantidadCuotas() {
        return cantidadCuotas;
    }

    public void setCantidadCuotas(int cantidadCuotas) {
        this.cantidadCuotas = cantidadCuotas;
    }

    public double getPorcentajePlanMensual() {
        return porcentajePlanMensual;
    }

    public void setPorcentajePlanMensual(double porcentajePlanMensual) {
        this.porcentajePlanMensual = porcentajePlanMensual;
    }

    public ArrayList<Credito> getCreditos() {
        return _creditos;
    }

    public void setCreditos(ArrayList<Credito> _creditos) {
        this._creditos = _creditos;
    }
}
