
package finance.modelo;

import java.util.ArrayList;

public class Repositorio {
    
    private ArrayList<Plan> planes = new ArrayList<>(); //Agregue
    private ArrayList<Cliente> clientes = new ArrayList<>(); //Agregue
    private static ArrayList<Empleado> empleados = new ArrayList<>(); //Agregue
    
    private static final Repositorio repositorio = new Repositorio();

    public Repositorio() {
        //crearPlanes();
        crearEmpleados();
        crearClientes();
    }

    public void crearPlanes(){
        planes.add(new Plan(3, 5, "1.Plan.CuotaAdelantada"));
        planes.add(new Plan(8, 10, "2.Plan.CuotaAdelantada"));
        planes.add(new Plan(12, 8, "3.Plan.CuotaVencida"));
        planes.add(new Plan(3, 5, "4.Plan.CuotaVencida"));
    }
    
    //Para obtener los planes cargados
    public static Plan getPlanes(String descripcion) {
        for(Plan p : Repositorio.getInstance().getPlanes()){
            if(p.getDescripcion().equalsIgnoreCase(descripcion)) return p;
        }
        return null;
    }

    public void crearEmpleados(){
        empleados.add(new Empleado(01, "Admin", "Admin"));
        empleados.add(new Empleado(02, "Invitado", "Invitado"));
        empleados.add(new Empleado(03, "Florencia", "Rodriguez"));
        empleados.add(new Empleado(04, "Juan", "Perez"));
    }
    
    public void crearClientes(){
        clientes.add(new Cliente(33000819, "Lionel", "Messi", "Rosario 675", 4356784, 120000));
        clientes.add(new Cliente(33000817, "Marcos", "Reus", "Alemania 500", 3428943, 45000));
        clientes.add(new Cliente(33000810, "Micaela", "Gomez", "Jujuy 675", 2124839, 50000));
        clientes.add(new Cliente(33000822, "Sol", "Avila", "Mendoza 675", 2347890, 30000));
    }
    
    public static Repositorio getInstance()
    {
        return Repositorio.repositorio;
    }

    //METODOS GET Y SET
    public ArrayList<Plan> getPlanes() {
        return planes;
    }

    public void setPlanes(ArrayList<Plan> planes) {
        this.planes = planes;
    }

    public ArrayList<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(ArrayList<Cliente> clientes) {
        this.clientes = clientes;
    }
    
    public static ArrayList<Empleado> getEmpleados() {
        return empleados;
    }

    public static void setEmpleados(ArrayList<Empleado> aEmpleados) {
        empleados = aEmpleados;
    }
    
    public void agregarCliente(Cliente c){
        clientes.add(c);
    }
    
    public void agregarEmpleado(Empleado e){
        empleados.add(e);
    }
    
    public void agregarPlanes(Plan p){
        planes.add(p);
    }
}
