
package finance.modelo;

import java.util.Calendar;


public class LineaDePago {
    private int nroLinea;
    private double monto;
    private Cuota cuota; //Agregue

    public LineaDePago() {
    }

    public LineaDePago(int nroLinea, Cuota cuota, double monto) {
        this.nroLinea = nroLinea;
        this.cuota = cuota;
        this.monto = monto;
    }
    
    public int getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(int nroLinea) {
        this.nroLinea = nroLinea;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public Cuota getCuota() {
        return cuota;
    }

    public void setCuota(Cuota cuota) {
        this.cuota = cuota;
    }
    
  public double subtotal(Cuota cuota, double monto){
      
      final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        if (cuota.getFechaVencimiento().getDia() > mDay && cuota.getFechaVencimiento().getMes() > mMonth) {
          int diasPasados = (cuota.getFechaVencimiento().getDia() - mDay)*(cuota.getFechaVencimiento().getMes() - mMonth);
          double resultado = (getMonto() - (cuota.getMontoCuota()*diasPasados*Financiera.getInstance().getInteresDiario()));
        cuota.setSaldo(resultado);
        return resultado;
      }
      return Double.parseDouble(null);
  }
    
}
