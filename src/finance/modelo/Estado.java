
package finance.modelo;

public enum Estado {
    pendiente,
    activo,
    moroso,
    pendienteDeFinalizacion,
    finalizado;

}
