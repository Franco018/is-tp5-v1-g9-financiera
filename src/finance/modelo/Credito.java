
package finance.modelo;

import java.util.ArrayList;
import java.util.Date;

public class Credito {
    private int nroCredito;
    private Fecha fechaEmision;
    private double montoSolicitado;
    private Estado estado; //Agregado
    private double montoTotal; //Agregue
    private Plan plan; //Agregue
    private ArrayList<Cuota> _cuotas = new ArrayList<>(); 

    public Credito() {
        Repositorio.getInstance().getPlanes();
    }

    public Credito(int nroCredito, double montoSolicitado) {
        this.nroCredito = nroCredito;
        this.montoSolicitado = montoSolicitado;

    }
    public int getNroCredito() {
        return nroCredito;
    }

    public void setNroCredito(int nroCredito) {
        this.nroCredito = nroCredito;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public double getMontoSolicitado() {
        return montoSolicitado;
    }

    public void setMontoSolicitado(double montoSolicitado) {
        this.montoSolicitado = montoSolicitado;
    }

    public double getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(double montoTotal) {
        this.montoTotal = montoTotal;
    }
    
    public Plan getPlan() {
        return plan;
    }

    public void setPlan(Plan plan) {
        this.plan = plan;
    }

    public Fecha getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(Fecha fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public ArrayList<Cuota> getCuotas() {
        return _cuotas;
    }

    public void setCuotas(ArrayList<Cuota> _cuotas) {
        this._cuotas = _cuotas;
    }
    
    //METODO
   public double OtorgarCredito(String descripcion, double monto){
       double totalCredito = 0;
       double ImporteGastoAdmin = 0;
       double importeOtorgado = 0;
       double importeCuota = 0;
       double montoMaximo = Financiera.getInstance().getMontoMaximo();
       System.out.println(" "+montoMaximo);
       
       if (monto <= montoMaximo) {
           for (Plan pla : Repositorio.getInstance().getPlanes()) {
           if (pla.getDescripcion().equalsIgnoreCase(descripcion) && (descripcion.equalsIgnoreCase("3.Plan.CuotaVencida")
                   || descripcion.equalsIgnoreCase("4.Plan.CuotaVencida"))) {
            PlanCuotaVencida pla1 = new PlanCuotaVencida(pla.getCantidadCuotas(), pla.getPorcentajePlanMensual(), pla.getDescripcion());
            pla1.setPorcentajeGastoAdmninistrativo(2);
            setMontoTotal((getMontoSolicitado() + ((getMontoSolicitado()* pla1.getPorcentajePlanMensual())/100)*pla1.getCantidadCuotas()));
            totalCredito = getMontoTotal();
            ImporteGastoAdmin = ((getMontoSolicitado()*pla1.getPorcentajeGastoAdmninistrativo())/100);
            importeCuota = (totalCredito/pla1.getCantidadCuotas());
            importeOtorgado = getMontoSolicitado() - ImporteGastoAdmin;
            
               System.out.println("Credito Otorgado"+
                    "\nMonto solicitado: "+getMontoSolicitado()+
                    "\nPlan de "+pla1.getCantidadCuotas()+" cuotas"+" - descripcion: "+pla1.getDescripcion()+
                    " - porcentaje gastos: "+pla1.getPorcentajeGastoAdmninistrativo()+
                    "\nImporte de gastos: $"+ImporteGastoAdmin+
                    " - Interes Mensual: "+pla1.getPorcentajePlanMensual()+
                    "\nMonto total del credito: $"+totalCredito+
                    "\nImporte de cuota: $"+importeCuota+
                    "\nImporte otorgado al cliente: $"+importeOtorgado);
            Cuota c = new Cuota(nroCredito, importeCuota, Financiera.getInstance().getInteresDiario(), new Fecha(29, 8, 2019));
            _cuotas.add(c);
            }
           else if (pla.getDescripcion().equalsIgnoreCase(descripcion) && (descripcion.equalsIgnoreCase("1.Plan.CuotaAdelantada")
                   || descripcion.equalsIgnoreCase("2.Plan.CuotaAdelantada"))) {
               PlanCuotaAdelantada pca = new PlanCuotaAdelantada(pla.getCantidadCuotas(), pla.getPorcentajePlanMensual(), pla.getDescripcion());
               setMontoTotal((getMontoSolicitado() + ((getMontoSolicitado()* pca.getPorcentajePlanMensual())/100)*pca.getCantidadCuotas()));
               totalCredito = getMontoTotal();
               importeCuota = (totalCredito/pca.getCantidadCuotas());
               importeOtorgado = getMontoSolicitado() - importeCuota;
            
               System.out.println("Credito Otorgado"+
                    "\nMonto solicitado: "+getMontoSolicitado()+
                    "\nPlan de "+pca.getCantidadCuotas()+" cuotas"+" - descripcion: "+pca.getDescripcion()+
                    " - Interes Mensual: "+pca.getPorcentajePlanMensual()+
                    "\nMonto total del credito: $"+totalCredito+
                    "\nImporte de cuota: $"+importeCuota+
                    "\nImporte de gastos: $"+ImporteGastoAdmin+
                    "\nImporte otorgado al cliente: $"+importeOtorgado);
            
             Cuota c = new Cuota(nroCredito, importeCuota, Financiera.getInstance().getInteresDiario(), new Fecha(29, 8, 2019));
            _cuotas.add(c);
           }
       }
       return importeOtorgado;
       }
       return Double.parseDouble(null);
   }

}
