
package finance.modelo;


public class Cuota {
    
    private int nroCuota;
    private double montoCuota;
    private double intereses;
    private double saldo;
    private Fecha fechaVencimiento;

    public Cuota() {
    }

    public Cuota(int nroCuota, double montoCuota, double intereses, Fecha fechaVencimiento) {
        this.nroCuota = nroCuota;
        this.montoCuota = montoCuota;
        this.intereses = intereses;
        this.fechaVencimiento = fechaVencimiento;
    }
    
    public double getIntereses() {
        return intereses;
    }

    public void setIntereses(double intereses) {
        this.intereses = intereses;
    }

    public int getNroCuota() {
        return nroCuota;
    }

    public void setNroCuota(int cuota) {
        this.nroCuota = cuota;
    }
    
    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public double getMontoCuota() {
        return montoCuota;
    }

    public void setMontoCuota(double montoCuota) {
        this.montoCuota = montoCuota;
    }

    public Fecha getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Fecha fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }
    
    
}
