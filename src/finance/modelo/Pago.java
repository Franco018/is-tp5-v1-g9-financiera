
package finance.modelo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Pago {
    private int nroPago;
    private double montoPago;
    private Fecha fechaPago;
    private Empleado empleado; //Agregue
    private ArrayList<LineaDePago> _lineaPago = new ArrayList<>(); //Agregue

    public Pago(int nroPago, Empleado empleado) {
        this.nroPago = nroPago;
        this.empleado = empleado;
    }

    public int getNroPago() {
        return nroPago;
    }

    public void setNroPago(int nroPago) {
        this.nroPago = nroPago;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }
    
    public double SubTotal(double montoPago, double saldo){
       
        for (LineaDePago lineaDePago : _lineaPago) {
            double subTotal= 0;
            if (saldo >= montoPago && lineaDePago.getCuota().getFechaVencimiento().getMes() < Calendar.DATE) {
                subTotal = (saldo - montoPago)*Financiera.getInstance().getInteresDiario();
                return subTotal;
            } else{
            System.out.println("Cuota pagada.");
        }
      }
        return Double.parseDouble(null);
    }
}
