
package finance.modelo;

import java.util.ArrayList;

public class Financiera {

    public static ArrayList<Empleado> getEmpleados() {
        return empleados;
    }

    public static void setEmpleados(ArrayList<Empleado> aEmpleados) {
        empleados = aEmpleados;
    }
    private String codigo = "980883a8-38cc-4193-96b8-182b44ee53af";
    private String nombreComercial;
    private String razonSocial;
    private int Cuit;
    private String domicilio;
    private double interesDiario;
    private double montoMaximo;
    private int cantidadMaximaCreditosActivos;
    private int cantidadCuotasImpagas;
    private ArrayList<Plan> planes = new ArrayList<>(); //Agregue
    private ArrayList<Cliente> clientes = new ArrayList<>(); //Agregue
    private static ArrayList<Empleado> empleados = new ArrayList<>(); //Agregue
    
    private static final Financiera financiera = new Financiera();

    public Financiera() {
    }

    public Financiera(String nombreComercial, String razonSocial, int CUIT, String domicilio, double interesDiario, double montoMaximo, int cantidadMaximaCreditosActivos, int cantidadCuotasImpagas) {
        this.nombreComercial = nombreComercial;
        this.razonSocial = razonSocial;
        this.Cuit = CUIT;
        this.domicilio = domicilio;
        this.interesDiario = interesDiario;
        this.montoMaximo = montoMaximo;
        this.cantidadMaximaCreditosActivos = cantidadMaximaCreditosActivos;
        this.cantidadCuotasImpagas = cantidadCuotasImpagas;
    }

    public static Financiera getInstance()
    {
        return Financiera.financiera;
    }

    //METODOS GET Y SET
    public ArrayList<Plan> getPlanes() {
        return planes;
    }

    public void setPlanes(ArrayList<Plan> planes) {
        this.planes = planes;
    }

    public ArrayList<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(ArrayList<Cliente> clientes) {
        this.clientes = clientes;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial = nombreComercial;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public int getCuit() {
        return Cuit;
    }

    public void setCuit(int Cuit) {
        this.Cuit = Cuit;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public double getInteresDiario() {
        return interesDiario;
    }

    public void setInteresDiario(double interesDiario) {
        this.interesDiario = interesDiario;
    }

    public double getMontoMaximo() {
        return montoMaximo;
    }

    public void setMontoMaximo(double montoMaximo) {
        this.montoMaximo = montoMaximo;
    }

    public int getCantidadMaximaCreditosActivos() {
        return cantidadMaximaCreditosActivos;
    }

    public void setCantidadMaximaCreditosActivos(int cantidadMaximaCreditosActivos) {
        this.cantidadMaximaCreditosActivos = cantidadMaximaCreditosActivos;
    }

    public int getCantidadCuotasImpagas() {
        return cantidadCuotasImpagas;
    }

    public void setCantidadCuotasImpagas(int cantidadCuotasImpagas) {
        this.cantidadCuotasImpagas = cantidadCuotasImpagas;
    }
}
