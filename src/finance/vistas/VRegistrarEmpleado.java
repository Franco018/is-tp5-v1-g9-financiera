
package finance.vistas;

import finance.Interface.IRegistrarEmpleado;
import finance.modelo.Empleado;
import finance.presentador.PresentadoGestionarEmpleado;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;


public class VRegistrarEmpleado extends javax.swing.JFrame implements IRegistrarEmpleado{

    private PresentadoGestionarEmpleado _presentador;
    private Empleado _empleado;
    private DefaultTableModel dm;

    public VRegistrarEmpleado() {
        initComponents();
        _presentador = new PresentadoGestionarEmpleado(this);
        _empleado = new Empleado();
    }

    public void ejecutar(){
        this.setTitle("Financiara.RegistrarEmpleado");
        this.setVisible(true);
        this.setLocationRelativeTo(null);
    }
   
    public void clear(){
    txtLegajo.setText("");
    txtNombre.setText("");
    txtApellido.setText("");
    }
    
    public void MostrarTabla(ArrayList<Empleado> empleados, JTable table){
		String [] titulos = {"Legajo", "Nombre", "Apellido"};
		String [] fila = new String[3];
		dm = new DefaultTableModel(null, titulos);
                dm.addRow(fila);
                
                for (int i = 0; i < empleados.size(); i++) {
                    Empleado getE = empleados.get(i);
                dm.setValueAt(getE.getLegajo(), i, 0);
                dm.setValueAt(getE.getNombre(), i, 1);
                dm.setValueAt(getE.getApellido(), i, 2);
            }
                tableEmpleado.setModel(dm);
	}	
    
    public void seleccionar(JTable table){
        int fila = table.getSelectedRow();
        if (fila >= 0) {
           txtLegajo.setText(tableEmpleado.getValueAt(fila, 0).toString());
           txtNombre.setText(tableEmpleado.getValueAt(fila, 1).toString());
           txtApellido.setText(tableEmpleado.getValueAt(fila, 2).toString());
        }else{MostrarMensaje("Seleccione una fila.");}
    }
    
     @Override
    public void RegistrarEmpleado(Empleado empleado) {
         MostrarTabla( _presentador.registrarCLiente(empleado), tableEmpleado);
    }
    
     @Override
    public void Modificar() {
        String dato = txtLegajo.getText();
        int fila = tableEmpleado.getSelectedRow();
        int col = tableEmpleado.getSelectedColumn();
        if (fila >= 0 && col >= 0) {
           dm.setValueAt(dato, fila, col);
        }else{MostrarMensaje("Seleccione una fila para modificar.");}
    }

    @Override
    public void Eliminar() {
        int fila = tableEmpleado.getSelectedRow();
        if (fila >= 0) {
           dm.removeRow(fila);
        }else{MostrarMensaje("Seleccione una fila para eliminar.");}
    }
    
        @Override
    public void MostrarMensaje(String text) {
            JOptionPane.showMessageDialog(null, text);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        MenuModificar = new javax.swing.JMenuItem();
        MenuEliminar = new javax.swing.JMenuItem();
        btnRegistrar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        btnSalir = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtLegajo = new javax.swing.JTextField();
        txtNombre = new javax.swing.JTextField();
        txtApellido = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableEmpleado = new javax.swing.JTable();
        btnModificar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();

        MenuModificar.setText("Modificar");
        MenuModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuModificarActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MenuModificar);

        MenuEliminar.setText("Eliminar");
        MenuEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuEliminarActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MenuEliminar);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnRegistrar.setText("Registrar");
        btnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Legajo");

        btnSalir.setText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Nombre");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel6.setText("REGISTRAR EMPLEADO");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Apellido");

        tableEmpleado.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Legajo", "Nombre", "Apellido"
            }
        ));
        tableEmpleado.setComponentPopupMenu(jPopupMenu1);
        jScrollPane1.setViewportView(tableEmpleado);

        btnModificar.setText("Modificar");

        btnEliminar.setText("Eliminar");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(52, 52, 52)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1))
                        .addGap(48, 48, 48)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtLegajo, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(38, 38, 38)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 581, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnRegistrar)
                .addGap(69, 69, 69)
                .addComponent(btnModificar)
                .addGap(78, 78, 78)
                .addComponent(btnEliminar)
                .addGap(71, 71, 71)
                .addComponent(btnSalir)
                .addGap(57, 57, 57))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(txtLegajo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnRegistrar)
                    .addComponent(btnModificar)
                    .addComponent(btnSalir)
                    .addComponent(btnEliminar))
                .addContainerGap(24, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
       dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarActionPerformed
        RegistrarEmpleado(new Empleado(Integer.parseInt(txtLegajo.getText()), txtNombre.getText(), txtApellido.getText()));
        clear();
    }//GEN-LAST:event_btnRegistrarActionPerformed

    private void MenuModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuModificarActionPerformed
        Modificar();
    }//GEN-LAST:event_MenuModificarActionPerformed

    private void MenuEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuEliminarActionPerformed
       Eliminar();
    }//GEN-LAST:event_MenuEliminarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JMenuItem MenuEliminar;
    public javax.swing.JMenuItem MenuModificar;
    public javax.swing.JButton btnEliminar;
    public javax.swing.JButton btnModificar;
    public javax.swing.JButton btnRegistrar;
    public javax.swing.JButton btnSalir;
    public javax.swing.JLabel jLabel1;
    public javax.swing.JLabel jLabel2;
    public javax.swing.JLabel jLabel3;
    public javax.swing.JLabel jLabel6;
    public javax.swing.JPopupMenu jPopupMenu1;
    public javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JTable tableEmpleado;
    public javax.swing.JTextField txtApellido;
    public javax.swing.JTextField txtLegajo;
    public javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables


}
