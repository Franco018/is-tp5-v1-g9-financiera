
package finance.vistas;

import finance.Interface.IAbonarCuota;
import finance.modelo.*;
import finance.presentador.PresentadorAbonarCuota;
import java.awt.Component;
import java.util.ArrayList;
import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

public class VAbonarCuotas extends javax.swing.JFrame implements IAbonarCuota{

    DefaultTableModel dm;
    PresentadorAbonarCuota _presentador;
    private Credito credito;
    
    public VAbonarCuotas() {
        initComponents();
        credito = new Credito();
        _presentador = new PresentadorAbonarCuota(this);
    }
    
    public void ejecutar(){
        this.setTitle("Financiera.Abonar cuota");
        this.setVisible(true);
        this.setLocationRelativeTo(null);
    }
    
    public void MostrarTabla(ArrayList<Cliente> clientes){
		String [] titulos = {"Nro credito", "Nro Cuota", "Monto", "Fecha vencimiento", "interes", 
                                    "Saldo", "Subtotal","A pagar"};
                
		String [] fila = new String[7];
		dm = new DefaultTableModel(null, titulos);
                dm.addRow(fila);
                
                tableLineaPago.getColumnModel().getColumn(7).setCellEditor(defaultcelleditor);
                tableLineaPago.setDefaultRenderer(tableLineaPago.getColumnClass(0), claserender);
                for (int i = 0; i < clientes.size(); i++) {
                    Cliente getC = clientes.get(i);
                    if (Integer.parseInt(txtDni.getText()) == getC.getDni()) {
                        dm.setValueAt(getC.getCreditos().get(i).getNroCredito(), i, 0);
                        dm.setValueAt(getC.getCreditos().get(i).getCuotas().get(i).getNroCuota(), i, 1);
                        dm.setValueAt(getC.getCreditos().get(i).getCuotas().get(i).getMontoCuota(), i, 2);
                        dm.setValueAt(getC.getCreditos().get(i).getCuotas().get(i).getFechaVencimiento().getToString(), i, 3);
                        dm.setValueAt(Financiera.getInstance().getInteresDiario(), i, 4);
                        dm.setValueAt(getC.getCreditos().get(i).getCuotas().get(i).getSaldo(), i, 5);
                        dm.setValueAt(getC.getCreditos().get(i).getCuotas().get(i).getMontoCuota(), i, 6);
                        dm.setValueAt(false, i, 7);
                    }
            }
                tableLineaPago.setModel(dm);
	}
    
     @Override
    public void informarCreditoFinalizado(int dni) {
        _presentador.informarCreditoFinalizado(dni);
    }

    @Override
    public void MostrarMensaje(String mje) {
        JOptionPane.showMessageDialog(null, mje);
    }
    
     @Override
    public void abonarCuota(Pago pago) {
        int fila = tableLineaPago.getSelectedRow();
        tableLineaPago.isCellSelected(1, 7);
    }
    
     @Override
    public void abonarCuotaParcial(double monto) {
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableLineaPago = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JTextField();
        btnBuscar = new javax.swing.JButton();
        btnConfirmar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        txtDni = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Dni");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel2.setText("ABONAR CUOTA");

        tableLineaPago.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nro Credito", "Nro Cuotal", "Monto", "Fecha vencimiento", "Interes", "Saldo", "Subtotal", "A pagar"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tableLineaPago);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("Total");

        btnBuscar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        btnConfirmar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnConfirmar.setText("Confirmar");
        btnConfirmar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmarActionPerformed(evt);
            }
        });

        btnSalir.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnSalir.setText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 41, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(41, 41, 41)
                        .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 575, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(40, 40, 40))
            .addGroup(layout.createSequentialGroup()
                .addGap(58, 58, 58)
                .addComponent(btnConfirmar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSalir)
                .addGap(62, 62, 62))
            .addGroup(layout.createSequentialGroup()
                .addGap(52, 52, 52)
                .addComponent(jLabel1)
                .addGap(26, 26, 26)
                .addComponent(txtDni, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(btnBuscar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(jLabel2)
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(btnBuscar)
                    .addComponent(txtDni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(66, 66, 66)
                        .addComponent(btnConfirmar)
                        .addGap(44, 44, 44))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnSalir)
                        .addGap(34, 34, 34))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    Render claserender = new Render();
    JCheckBox check = new JCheckBox();
    DefaultCellEditor defaultcelleditor = new DefaultCellEditor(check);
    
    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        ArrayList<Cliente> clientes = _presentador.buscarCliente(Integer.parseInt(txtDni.getText()));
        MostrarTabla(clientes);
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnConfirmarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmarActionPerformed
        informarCreditoFinalizado(Integer.parseInt(txtDni.getText()));
    }//GEN-LAST:event_btnConfirmarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnConfirmar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JTable tableLineaPago;
    public javax.swing.JTextField txtDni;
    public javax.swing.JTextField txtTotal;
    // End of variables declaration//GEN-END:variables

    
    public class Render implements TableCellRenderer{

        @Override
        public Component getTableCellRendererComponent(JTable jtable, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            JLabel label = new JLabel();
            JCheckBox check = new JCheckBox();
            
            DefaultTableModel model = (DefaultTableModel) jtable.getModel();
            
            if (model.getValueAt(row, column).getClass().equals(Boolean.class)) {
                check.setSelected(Boolean.parseBoolean(model.getValueAt(row, column).toString()));
                return check;
            }
            
            if (column != 2) {
                label.setText(model.getValueAt(row, column).toString());
            }
            
            return label;
        }
        
    }
}
