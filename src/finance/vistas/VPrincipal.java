
package finance.vistas;

public class VPrincipal extends javax.swing.JFrame {
    
    public VPrincipal(String usuario, int legajo) {
        initComponents();
        lejago.setText(Integer.toString(legajo));
        Empleado.setText(usuario);
    }
    public void ejecutar(){
        this.setTitle("Financiera.Ventana principal");
        this.setVisible(true);
        this.setLocationRelativeTo(null);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lejago = new javax.swing.JLabel();
        Empleado = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        Menu_RegCliente = new javax.swing.JMenuItem();
        Menu_RegEmpleado = new javax.swing.JMenuItem();
        Menu_SolicitarCredito = new javax.swing.JMenuItem();
        MenuAbonarCuota = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        menuSalir = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lejago.setFont(new java.awt.Font("Verdana", 1, 18)); // NOI18N
        lejago.setText("jLabel1");

        Empleado.setFont(new java.awt.Font("Verdana", 1, 18)); // NOI18N
        Empleado.setText("jLabel2");

        jLabel1.setFont(new java.awt.Font("Verdana", 1, 18)); // NOI18N
        jLabel1.setText("Legajo:");

        jLabel2.setFont(new java.awt.Font("Verdana", 1, 18)); // NOI18N
        jLabel2.setText("Empleado:");

        jMenuBar1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N

        jMenu1.setText("Archivo");

        Menu_RegCliente.setText("Registrar cliente");
        Menu_RegCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Menu_RegClienteActionPerformed(evt);
            }
        });
        jMenu1.add(Menu_RegCliente);

        Menu_RegEmpleado.setText("Registrar empleado");
        Menu_RegEmpleado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Menu_RegEmpleadoActionPerformed(evt);
            }
        });
        jMenu1.add(Menu_RegEmpleado);

        Menu_SolicitarCredito.setText("Solicitar credito");
        Menu_SolicitarCredito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Menu_SolicitarCreditoActionPerformed(evt);
            }
        });
        jMenu1.add(Menu_SolicitarCredito);

        MenuAbonarCuota.setText("Abonar cuota");
        MenuAbonarCuota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuAbonarCuotaActionPerformed(evt);
            }
        });
        jMenu1.add(MenuAbonarCuota);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Salir");

        menuSalir.setText("Salir programa");
        menuSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSalirActionPerformed(evt);
            }
        });
        jMenu2.add(menuSalir);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(69, 69, 69)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addGap(57, 57, 57)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lejago, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(Empleado, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap(94, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(94, 94, 94)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lejago)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Empleado)
                    .addComponent(jLabel2))
                .addContainerGap(121, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void menuSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSalirActionPerformed
        dispose();
    }//GEN-LAST:event_menuSalirActionPerformed

    private void Menu_RegClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Menu_RegClienteActionPerformed
       new VRegistrarCliente().ejecutar();
    }//GEN-LAST:event_Menu_RegClienteActionPerformed

    private void Menu_RegEmpleadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Menu_RegEmpleadoActionPerformed
        new VRegistrarEmpleado().ejecutar();
    }//GEN-LAST:event_Menu_RegEmpleadoActionPerformed

    private void Menu_SolicitarCreditoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Menu_SolicitarCreditoActionPerformed
        new VSolicitarCredito().ejecutar();
    }//GEN-LAST:event_Menu_SolicitarCreditoActionPerformed

    private void MenuAbonarCuotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuAbonarCuotaActionPerformed
        new VAbonarCuotas().ejecutar();
    }//GEN-LAST:event_MenuAbonarCuotaActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JLabel Empleado;
    public javax.swing.JMenuItem MenuAbonarCuota;
    public javax.swing.JMenuItem Menu_RegCliente;
    public javax.swing.JMenuItem Menu_RegEmpleado;
    public javax.swing.JMenuItem Menu_SolicitarCredito;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    public javax.swing.JMenu jMenu1;
    public javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    public javax.swing.JLabel lejago;
    public javax.swing.JMenuItem menuSalir;
    // End of variables declaration//GEN-END:variables
}
