
package finance.presentador;

import finance.Interface.IRegistrarEmpleado;
import finance.modelo.Empleado;
import finance.modelo.Repositorio;
import java.util.ArrayList;

public class PresentadoGestionarEmpleado {
    
    private IRegistrarEmpleado _vistaRegistrarEmpleado;
    private ArrayList<Empleado> empleados = new ArrayList<>();

    public PresentadoGestionarEmpleado(IRegistrarEmpleado vistaRegistrarEmpleado) {
        this._vistaRegistrarEmpleado = vistaRegistrarEmpleado;
    }
    
    public ArrayList<Empleado> registrarCLiente(Empleado _empleado){
           empleados.add(_empleado);
           Repositorio.getInstance().agregarEmpleado(_empleado);
            _vistaRegistrarEmpleado.MostrarMensaje("Empleado Registrado: "+
                    "\nLegajo: "+_empleado.getLegajo()+
                    "\nNombre: "+_empleado.getNombre()+ " Apellido: "+_empleado.getApellido());
            return empleados;
    }
}
