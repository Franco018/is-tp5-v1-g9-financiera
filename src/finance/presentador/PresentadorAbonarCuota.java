
package finance.presentador;

import finance.Interface.IAbonarCuota;
import finance.modelo.*;
import finance.ws.ServicioExterno;
import java.util.ArrayList;

public class PresentadorAbonarCuota {
    
    private IAbonarCuota _vistaAbonarCuota;
    private ArrayList<LineaDePago> linea = new ArrayList<LineaDePago>();
    private ArrayList<Cliente> clientes = Repositorio.getInstance().getClientes();
    private ServicioExterno ws;
    
    public PresentadorAbonarCuota(IAbonarCuota vistaAbonarCuota) {
        this._vistaAbonarCuota = vistaAbonarCuota;
         ws = new ServicioExterno();
    }
    
    public ArrayList<Cliente> buscarCliente(int dni){
        for (Cliente cliente : clientes) {
            if (dni == cliente.getDni()) {
                return clientes;
            }
        }
        return null;
    }
    
    public void registrarPago(int dni){
        int i = 0;
        for (Cliente cliente : clientes) {
            if (dni == cliente.getDni()) {
                for (LineaDePago lineaDePago : linea) {
                    lineaDePago.subtotal(cliente.getCreditos().get(i).getCuotas().get(i), dni);
                }
            }
        }
    }
    
    public void informarCreditoFinalizado(int dni){
          int i = 0;
        for (Cliente cliente : clientes) {
            if (dni == cliente.getDni()) {
                ws.InformarCreditoFinalizado(dni, Integer.toString(cliente.getCreditos().get(i).getNroCredito()));
            }
            i++;
        }
    }
}
