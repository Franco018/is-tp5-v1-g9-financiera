
package finance.presentador;

import finance.Interface.ISolicitarCredito;
import finance.modelo.*;
import finance.ws.ServicioExterno;
import java.util.ArrayList;

public class PresentadorSolicitarCredito {
    
    private ISolicitarCredito _vistaSolicitarCredito;
    private ArrayList<Plan> planes = new ArrayList<Plan>();
    private ArrayList<Credito> _credito = new ArrayList<Credito>();
    private ServicioExterno ws;
    private ArrayList<Cliente> _clientes;

    public PresentadorSolicitarCredito(ISolicitarCredito vistaSolicitarCredito) {
        this._vistaSolicitarCredito = vistaSolicitarCredito;
        this.planes = Repositorio.getInstance().getPlanes();
        ws = new ServicioExterno();
        _clientes = Repositorio.getInstance().getClientes();
        this._vistaSolicitarCredito.cargarPlanes(this.planes);
    }
    
    public void ingresarCliente(int dni){
        ws.ObtenerEstadoCliente(dni);
    }
    
    //METODO PARA VERIFICAR SI SE LE PUEDE OTORGAR EL CREDITO AL CLIENTE
    public boolean validarCreditoOtorgar(int dni){
      if (ws.OtorgarCredito(dni)) {
           _vistaSolicitarCredito.MostrarMensaje("El cliente puede solicitar un credito.");
           return true;
        }
      return false;
    }
    
    //METODO PARA INFORMAR AL BANCO CENTRAL
    public void informarCreditoOtorgado(int dni, String nroCredito){
        ws.InformarCreditoFinalizado(dni, nroCredito);
    }
    
   //METODO PARA OTORGAR EL CREDITO AL CLIENTE
    public double OtorgarCredito(String descripcion, Credito credito){
        for (Cliente clie : _clientes) {
           clie.agregarCreditos(credito);
        }
        return credito.OtorgarCredito(descripcion, credito.getMontoSolicitado()); 
    }
}
