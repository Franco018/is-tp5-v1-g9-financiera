
package finance.presentador;

import finance.Interface.IRegistrarCliente;
import finance.modelo.Cliente;
import finance.modelo.Repositorio;
import java.util.ArrayList;

public class PresentadorGestionarCliente {
    
    private IRegistrarCliente _VistaRegistra;
    private ArrayList<Cliente> clientes = new ArrayList<>();

    public PresentadorGestionarCliente(IRegistrarCliente VistaRegistra) {
        this._VistaRegistra = VistaRegistra;
    }
    
        public ArrayList<Cliente> registrarCLiente(Cliente _cliente){
           clientes.add(_cliente);
            Repositorio.getInstance().agregarCliente(_cliente);
            _VistaRegistra.MostrarMensaje("Cliente Registrado: "+
                    "\nNombre: "+_cliente.getNombre()+ " Apellido: "+_cliente.getApellido()+
                    "\nDni: "+_cliente.getDni()+
                    "\nDomicilio: "+_cliente.getDomicilio()+
                    "\nTelefono: "+_cliente.getTelefono()+
                    "\nSueldo: "+_cliente.getSueldo());
            return clientes;
    } 
}
