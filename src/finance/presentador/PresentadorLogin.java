
package finance.presentador;

import finance.Interface.ILogin;
import finance.modelo.*;
import java.util.ArrayList;

public class PresentadorLogin {
    
    private ILogin _vistaLogin;
    private ArrayList<Empleado> empleados = new ArrayList<>();

    public PresentadorLogin(ILogin vistaLogin) {
        this._vistaLogin = vistaLogin;
        this.empleados = Repositorio.getInstance().getEmpleados();
    }
    
    public boolean IniciarSesion(String usuario, String contraseña){
        String pass = "1234";  //Usuarios: Juan, Florencia, Admin, Invitado.
        for (Empleado empleado : empleados) {
            if (empleado.getNombre().equalsIgnoreCase(usuario) && pass.equalsIgnoreCase(contraseña)) {
            _vistaLogin.MostrarMensaje("Bienvenido "+empleado.getNombre()+" "+empleado.getApellido());
            return true;
            }
        }
        return false;
    }
}
