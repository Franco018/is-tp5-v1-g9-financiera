
package finance.ws;

import finance.modelo.Financiera;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import ws.IServicioPublicoCredito;
import ws.IServicioPublicoCreditoInformarCreditoFinalizadoErrorServicioFaultFaultMessage;
import ws.IServicioPublicoCreditoInformarCreditoOtorgadoErrorServicioFaultFaultMessage;
import ws.IServicioPublicoCreditoObtenerEstadoClienteErrorServicioFaultFaultMessage;
import ws.ObtenerEstadoCliente;
import ws.ResultadoEstadoCliente;
import ws.ResultadoOperacion;
import ws.ServicioPublicoCredito;

public class ServicioExterno {
    
    ServicioPublicoCredito servicio = new ServicioPublicoCredito();
    IServicioPublicoCredito ws = servicio.getSGEBusService();
        
     public ServicioExterno() {
        servicio = new ServicioPublicoCredito();
        ws = servicio.getSGEBusService();
    }
     
    private String codigoFinanciera  = "980883a8-38cc-4193-96b8-182b44ee53af";
    
    //METODO QUE EVALUA EL ESTADO DE UN CLIENTE
    public ResultadoEstadoCliente ObtenerEstadoCliente(int dni)
    {
        ws.ObtenerEstadoCliente cliente = new ObtenerEstadoCliente();
        cliente.getDni();
        cliente.getCodigoFinanciera();
        ResultadoEstadoCliente res;
        try {
           res = ws.obtenerEstadoCliente(codigoFinanciera, dni);
            JOptionPane.showMessageDialog(null, "Resultado del estado del cliente: "+
                    "\nCantidad de creditos activos: "+res.getCantidadCreditosActivos()+
                    "\nTiene deudas: "+res.isTieneDeudas()+
                    "\nConsulta valida: "+res.isConsultaValida());
           return res;
        } catch (IServicioPublicoCreditoObtenerEstadoClienteErrorServicioFaultFaultMessage ex) {
            Logger.getLogger(ServicioExterno.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null,"Error al caargar estado del cliente.");
        }
        return null;
    }
    
    //METODO QUE EVALUA QUE TENGA MENOS DE 3 CREDITOS ACTIVOS Y NO TENGA DEUDAS
    public boolean OtorgarCredito(int dni){
        ws.ObtenerEstadoCliente cliente = new ObtenerEstadoCliente();
        cliente.getDni();
        cliente.getCodigoFinanciera();
        ResultadoEstadoCliente res;
        try {
           res = ws.obtenerEstadoCliente(codigoFinanciera, dni);
            if (res.getCantidadCreditosActivos() < Financiera.getInstance().getCantidadMaximaCreditosActivos()
                && res.isTieneDeudas()== false ) {
                return true;
            }
            else {return false;}
        } catch (IServicioPublicoCreditoObtenerEstadoClienteErrorServicioFaultFaultMessage ex) {
            Logger.getLogger(ServicioExterno.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null,"Error al realizar operacion.");
        }
        return false;
    }
    
    //METODO PARA INFORMAR AL BANCO CENTRAL QUE SE ENTREGO CREDITO
     public ResultadoOperacion InformarCreditoOtorgado(int dni, String identificadorCredito, double montoOtorgado)
        {
            ResultadoOperacion res;
            try {
                res = ws.informarCreditoOtorgado(codigoFinanciera, dni, identificadorCredito, montoOtorgado);
                JOptionPane.showMessageDialog(null, "Resultado del credito: "+
                    "\nCredito informado al banco central: "+res.isOperacionValida());
                return res;
            } catch (IServicioPublicoCreditoInformarCreditoOtorgadoErrorServicioFaultFaultMessage ex) {
                Logger.getLogger(ServicioExterno.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }

        //METODO SERVICIO PARA INFORMAR QUE EL CREDITO SE FINALIZO
        public ResultadoOperacion InformarCreditoFinalizado(int dni, String identificadorCredito)
        {
            ResultadoOperacion res;
            try {
                res = ws.informarCreditoFinalizado(codigoFinanciera, dni, identificadorCredito);
                JOptionPane.showMessageDialog(null, "Estado del credito: "+
                    "\nCredito estado finalizado: "+res.isOperacionValida());
                return res;
            } catch (IServicioPublicoCreditoInformarCreditoFinalizadoErrorServicioFaultFaultMessage ex) {
                Logger.getLogger(ServicioExterno.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }
}
