
package Pruebas;

import finance.modelo.*;
import finance.presentador.*;
import finance.vistas.*;
import finance.ws.ServicioExterno;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import ws.*;

public class PruebasUnidad {
    
    public PruebasUnidad() {
    }
    
    @BeforeClass
    public static void setUpClass() {
       // es invocado una vez al principio de todas los test. Se suele usar para inicializar atributos.
       Financiera.getInstance().setMontoMaximo(100000);
       Financiera.getInstance().getCodigo();
       Repositorio.getInstance().crearPlanes();
       Repositorio.getInstance().crearEmpleados();
       Repositorio.getInstance().crearClientes();
    }
    
    @AfterClass
    public static void tearDownClass() {
        // se invoca al finalizar todos los test.
    }
    
    @Before
    public void setUp() {
        // Se ejecuta antes de de cada test.
    }
    
    @After
    public void tearDown() {
        //Se ejecuta después de cada test.
    }

    // TEST:
    
     @Test
     public void TestImporteOtorgargadoAunClienteConPlanCuotaVencida() {
         // Representa un test que se debe ejecutar. 
        Credito c = new Credito();
        c.setMontoSolicitado(10000);
        String descripcion = "4.Plan.CuotaVencida";
        double monto = 10000;
        double esperado = 9800.0;
        double resultado = c.OtorgarCredito(descripcion, monto);
         
        assertEquals(esperado, resultado,0.0);
     }
     
     @Test
     public void TestInicioSesionEmpleado(){
         PresentadorLogin presentador = new PresentadorLogin(new Vlogin());
         
         boolean esperado = true;
         boolean resultado = presentador.IniciarSesion("Admin", "1234");
         
         assertEquals(esperado, resultado);
     }
     
     @Test
     public void TestBuscarClienteEnLaFinanciera(){
         PresentadorAbonarCuota presentador = new PresentadorAbonarCuota(new VAbonarCuotas());
         ArrayList<Cliente> clientes = Repositorio.getInstance().getClientes();
         for (Cliente cliente : clientes) {
             if (cliente.getDni() == 33000819) {
                 ArrayList<Cliente> esperado = clientes;
                 ArrayList<Cliente> resultado = presentador.buscarCliente(33000819);
                  assertEquals(esperado, resultado);
             }
         }
     }
     
     @Test
     public void TestRechazoAlInformarCreditoFinalizadoAlBancoCentral(){
         ServicioPublicoCredito servicio = new ServicioPublicoCredito();
         IServicioPublicoCredito ws = servicio.getSGEBusService();
         Boolean Resultado=true;
        try {
            Resultado = ws.informarCreditoFinalizado(Financiera.getInstance().getCodigo(), 33000819, Integer.toString(1)).isOperacionValida();
        } catch (IServicioPublicoCreditoInformarCreditoFinalizadoErrorServicioFaultFaultMessage ex) {
            Logger.getLogger(PruebasUnidad.class.getName()).log(Level.SEVERE, null, ex);
        }
         
         Boolean esperado = false;
         
         assertEquals(esperado, Resultado);
     }
     
     @Test
     public void TestValidarQueUnClienteNoPoseaDeudas(){
         ServicioExterno ws = new ServicioExterno();
         Boolean Resultado= ws.ObtenerEstadoCliente(33000819).isConsultaValida()== true && ws.ObtenerEstadoCliente(33000819).isTieneDeudas()==false;
         Boolean esperado = true;
         assertEquals(esperado, Resultado);
     }
}
